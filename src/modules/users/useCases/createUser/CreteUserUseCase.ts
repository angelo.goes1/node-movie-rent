import { AppError } from './../../../../errors/AppError';
import { User } from "@prisma/client";
import { prisma } from "../../../../prisma/client";
import { CreateUserDTO } from "./../../dto/CreateUserDTO";
export class CreateUserUseCase {
  async execute({ name, email }: CreateUserDTO): Promise<User>{
    // Verifica se usuário existe
    const userAlreadyExists = await prisma.user.findUnique({
      where: {
        email,
      },
    });

    // Erro
    if (userAlreadyExists) {
      throw new AppError("User already exists!");
    }

    // Criar usuário
    const user = await prisma.user.create({
        data: {
            name,
            email
        }
    });

    return user;
  }
}
