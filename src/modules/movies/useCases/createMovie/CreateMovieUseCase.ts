import { AppError } from '../../../../errors/AppError';
import { Movie } from "@prisma/client";
import { prisma } from "../../../../prisma/client";
import { CreateMovieDTO } from "../../dto/CreateMovieDTO";
export class CreateMovieUseCase {
  async execute({ title, duration, release_date }: CreateMovieDTO): Promise<Movie>{
    // Verifica se filme já existe
    const movieAlreadyExists = await prisma.movie.findUnique({
      where: {
        title,
      },
    });

    // Erro
    if (movieAlreadyExists) {
      throw new AppError("Movie already exists!");
    }

    // Criar usuário
    const movie = await prisma.movie.create({
        data: {
            title,
            duration,
            release_date
        }
    });

    return movie;
  }
}
